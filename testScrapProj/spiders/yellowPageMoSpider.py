# -*- coding: utf-8 -*-
import scrapy
import re
import time
import geocoder

class YellowpagemospiderSpider(scrapy.Spider):
    name = 'yellowPageMoSpider'
    allowed_domains = ['yp.mo']
    start_urls = ['http://yp.mo/business/購物/']

    def parse(self, response):
        subCats = response.xpath('//header[@class="sub-categories"]/.//li/a/@href').extract()

        for subCat in subCats:
            subCatURL = 'http://yp.mo'+"/"+ subCat
            #yield{'subCatUrl': subCatURL}
            yield scrapy.Request(subCatURL, callback=self.parse_sub)
        pass

    def parse_sub(self, response):

        articles= response.xpath('//article[contains(@class,"business-item")]')
        category  = " ".join(response.xpath('//h2/text()').extract()).strip()
        subcategory = response.xpath('//header[@class="sub-categories"]/.//a[@class="active"]/text()').extract_first()
        for article in articles:
            business = " ".join(article.xpath('.//dl/dt/@title').extract()).strip()
            add_list = article.xpath('.//dl/dd/text()').extract()
            address_eng= None
            address_chi= None
            phone = None
            for item in add_list:
                #isEngAdd = engAddressRegex.search(item)
                if re.search(r'[a-zA-Z][a-zA-Z][a-zA-Z][a-zA-Z][a-zA-Z]',item):
                    address_eng = item
                elif re.search(r'\d\d\d\d\d',item):
                    phone = item
                elif re.search(u'[\u4e00-\u9fff]+',item):
                    address_chi = item
            urlHref =article.xpath('.//dl/dt/a/@href').extract_first()
            absUrlHref = response.urljoin(urlHref)
            #email = ''
            lat = ''
            lng = ''
            print('-----------------------'+address_chi)
            geocod = geocoder.arcgis(u'澳門'+address_chi)
            print('-----------------------'+str(geocod))
            if geocod:
                lat =geocod.latlng[0]
                lng =geocod.latlng[1]
            yield{
                'category': category,
                'subcategory': subcategory,
                'business':business,
                'address_eng':address_eng,
                'address_chi':address_chi,
                'phone' : phone,
                'urlHref' : absUrlHref,
                'lat' : lat,
                'lng' : lng
            }


        next_pg_button = response.xpath('//button[contains(@class,"next-page-btn") and contains(@class,"fr")]/@onclick').extract_first()
        #yield{'nb':next_pg_button}
        pageMatch =  re.findall(r'page=\d*',next_pg_button)
        # yield{'pm':pageMatch}

        pos = response.url.find("?")
        if pos >0:
            base_url = response.url[0:pos]
        else:
            base_url = response.url
        if pageMatch:
            absolute_next_page_url = base_url+"?"+ pageMatch[0]
        time.sleep(2)
        #yield {'ab':absolute_next_page_url}

        yield scrapy.Request(absolute_next_page_url,callback=self.parse_sub)
